import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.giamSP(item);
              }}
              className="btn btn-primary mr-1"
            >
              -
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.tangSP(item);
              }}
              className="btn btn-primary ml-1"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 80 }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Price</td>
            <td>Quanity</td>
            <td>Image</td>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
