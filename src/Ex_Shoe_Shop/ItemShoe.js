import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3 p-1 h-">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title h-50">{name}</h4>
            <button
              onClick={() => this.props.handleAddToCart(this.props.data)}
              className="btn btn-success mr-1"
            >
              Mua
            </button>
            <button
              onClick={() => this.props.handleViewDetail(this.props.data)}
              className="btn btn-primary"
            >
              Xem chi tiet
            </button>
          </div>
        </div>
      </div>
    );
  }
}
