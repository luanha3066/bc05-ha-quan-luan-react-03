import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import Detail from "./Detail";
import ListShoe from "./ListShoe";
import Cart from "./Cart";
export default class Ex_Shoe_Shop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({
      detail: value,
    });
  };
  handleAddToCart = (item) => {
    let copyCart = [...this.state.cart];
    let index = this.state.cart.findIndex((shoe) => {
      return item.id == shoe.id;
    });
    // Neu chua co giay trong gio hang
    if (index == -1) {
      let CardItem = { ...item, number: 1 };
      copyCart.push(CardItem);
    } else {
      // Neu da co giay nay trong gio hang
      copyCart[index].number++;
    }
    this.setState({ cart: copyCart });
  };
  handleTangSP = (item) => {
    let cloneCard = [...this.state.cart];
    let index = this.state.cart.findIndex((shoe) => {
      return shoe.id === item.id;
    });
    cloneCard[index].number++;
    this.setState({ cart: cloneCard });
  };
  handleGiamSP = (item) => {
    let cloneCard = [...this.state.cart];
    let index = this.state.cart.findIndex((shoe) => {
      return shoe.id === item.id;
    });
    cloneCard[index].number--;
    if (cloneCard[index].number <= 0) {
      cloneCard.splice(index, 1);
    }
    this.setState({ cart: cloneCard });
  };
  render() {
    return (
      <div className="container">
        <Cart
          cart={this.state.cart}
          tangSP={this.handleTangSP}
          giamSP={this.handleGiamSP}
        />
        <ListShoe
          shoeArr={this.state.shoeArr}
          handleChangeDetail={this.handleChangeDetail}
          handleAddToCart={this.handleAddToCart}
        />
        <Detail detail={this.state.detail} />
      </div>
    );
  }
}
